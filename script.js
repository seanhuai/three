// import THREE from 'THREE';

function init() {
  const scene = new THREE.Scene();
    
  const renderer = new THREE.WebGLRenderer({antialias:true});
  renderer.setSize(window.innerWidth, window.innerHeight);
  renderer.setPixelRatio(window.devicePixelRatio); 
  renderer.setClearColor(0x000000, 0.8);

  const camera = new THREE.PerspectiveCamera(85, window.innerWidth / window.innerHeight, 0.1, 1000);
  camera.position.x = 20;
  camera.position.y = 18;
  camera.position.z = 35;
  camera.lookAt(scene.position); 

  const light = new THREE.DirectionalLight(0xffffff, 1.0, 0);
  light.position.set( 50, 50, 100 );
  scene.add(light);

  const cubeBox = new THREE.BoxGeometry(20, 20, 20);
  const cubeMater = new THREE.MeshPhongMaterial({specular: 0x000000});
  const cube = new THREE.Mesh(cubeBox, cubeMater);
  scene.add(cube);

  function render() {
    requestAnimationFrame(render);
    update();
    renderer.render(scene, camera);
  }

  function update() {
    cube.rotation.x += 0.005;
    cube.rotation.y += 0.01;
  }

  render();
  document.body.appendChild(renderer.domElement);
}

init();